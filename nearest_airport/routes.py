from flask import flash
from flask import render_template

from . import app
from .airports import Airports
from .forms import CoordinatesForm


airports = Airports.from_csv(app.config["AIRPORT_FILE"])


@app.route("/", methods=["GET", "POST"])
def index():
    """Return an HTML form which accepts lat and long input.

    On successful input validation display the nearest airport.
    """
    form = CoordinatesForm()
    closest = None
    if form.validate_on_submit():
        latitude = form.latitude.data
        longitude = form.longitude.data
        closest = airports.closest(latitude, longitude)
        flash(f"The closest aiport is {closest.name}: {closest.latitude}, "
              f"{closest.longitude}")
    return render_template(
        "index.html",
        form=form,
        closest_airport=closest
    )
