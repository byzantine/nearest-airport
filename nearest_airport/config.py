import os


class Config(object):
    TESTING = False
    SECRET_KEY = "c3409db08895851be80027a680fa2b01820b499fff5919aefbb3bdd694f5b27c"
    AIRPORT_FILE = os.environ["AIRPORT_FILE"]


class DevelopmentConfig(Config):
    pass
