import csv
import math
from decimal import Decimal


def lower_keys(**kwargs):
    """Copy of provided dictionary with lowered keys."""
    return {k.lower(): v for k, v in kwargs.items()}


class Airport:

    def __init__(self, name, icao, latitude, longitude):
        self.name = name
        self.icao = icao
        self.latitude = Decimal(latitude)
        self.longitude = Decimal(longitude)

    @classmethod
    def from_dict(cls, **kwargs):
        return cls(**lower_keys(**kwargs))

    def __repr__(self):
        return (f"Airport('{self.name}', '{self.icao}', {self.latitude}, "
                f"{self.longitude})")


class Airports(list):

    @classmethod
    def from_csv(cls, airport_file):
        with open(airport_file) as csv_file:
            airport_data = csv.DictReader(csv_file)
            return cls([Airport.from_dict(**row) for row in airport_data])

    def closest(self, latitude: Decimal, longitude: Decimal):
        """Return aiport closest to given coordinates."""
        closest = 10000, None
        for idx, airport in enumerate(self):
            dist = math.hypot(
                latitude - airport.latitude,
                longitude - airport.longitude,
            )
            if dist < closest[0]:
                closest = dist, airport
        return closest[1]
