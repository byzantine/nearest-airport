from flask import Flask
from .config import DevelopmentConfig

app = Flask(__name__)
app.config.from_object(DevelopmentConfig)

# Imported here to avoid circular import.
from . import routes
