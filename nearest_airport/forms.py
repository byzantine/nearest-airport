from flask_wtf import FlaskForm
from wtforms.fields import DecimalField
from wtforms.fields import SubmitField


class CoordinatesForm(FlaskForm):
    """Form for the display and validation of coordinates."""

    latitude = DecimalField("Latitude", places=6)
    longitude = DecimalField("Longitude", places=6)
    submit = SubmitField("Submit")
