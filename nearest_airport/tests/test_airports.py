import unittest
from decimal import Decimal

from nearest_airport.airports import lower_keys
from nearest_airport.airports import Airport
from nearest_airport.airports import Airports


class AirportTest(unittest.TestCase):

    def test_lower_keys(self):
        self.assertEqual(
            lower_keys(**{"HEATHROW": 1, "GATWICK": 2}),
            {"heathrow": 1, "gatwick": 2}
        )

    def test_from_dict_and_repr(self):
        airport = Airport.from_dict(
            **{'NAME': 'HAWARDEN', 'ICAO': 'EGNR', 'Latitude': '53.178056', 'Longitude': '-2.977778'}
        )
        self.assertEqual(
            str(airport),
            "Airport('HAWARDEN', 'EGNR', 53.178056, -2.977778)",
        )


class AirportsTest(unittest.TestCase):

    def test_from_csv(self):
        airports = Airports.from_csv(
            "nearest_airport/tests/nz_airport_coords.csv"
        )
        self.assertEqual(len(airports), 3)

    def test_closest(self):
        airports = Airports.from_csv(
            "nearest_airport/tests/nz_airport_coords.csv"
        )
        airport = airports.closest(Decimal(50.4616238), Decimal(-3.5376171))
        self.assertEqual(airport.name, "AUCKLAND")
        airport = airports.closest(Decimal(-77.8400829), Decimal(166.64453))
        self.assertEqual(airport.name, "CHRISTCHURCH")


if __name__ == '__main__':
    unittest.main()
