# To run the application

Create a Python virtual env and install the dependencies:
```python
python3 -m virtualenv venv
source venv/bin/activate
(venv) $ pip install -r requirements.txt
```

Create environment variables.  `AIRPORT_FILE` can be an absolute path or relative to the app.
```python
export AIRPORT_FILE=uk_airport_coords.csv
export FLASK_APP=nearest_airport/__init__.py
```

Finally, run the application:
```python
flask run
```
By default the application will run on http://127.0.0.1:5000.  Open the URL in your browser to enter a coordinate pair.


# To test

```python
source venv/bin/activate
python -m unittest
```
